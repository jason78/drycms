<?php
/*用户组的权限*/
$tempList = $this->get(SERVICE_GROUPPERMISSION)->getMoreByGroup($groupId);
$list = [];
foreach($tempList as $v){
    $list[] = str_replace('*', $module, $v->dry_route_name);
}
if(in_array($route, $list)){
    return true;
}
/*自动化*/
if($controller == 'Automate'){
    $model = $this->get(SERVICE_MODEL)->getModelByAlias($data['alias']);
    $route = "/{$module}/{$model->dry_controller_name}/{$action}";
    if(in_array($route, $list)){
        return true;
    }
}