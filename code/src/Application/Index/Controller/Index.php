<?php
namespace Application\Index\Controller;

class Index extends Base
{

    public function init()
    {
        $this->setDir('index');
    }

    public function indexAction()
    {
        $header = $this->getHeaderData();
        $host = strtolower($header['host']);
        $map = getConfig('host', THE_DEFAULT);
        if(isset($map[$host])){
            $this->jump($map[$host]);
        }
    }

}