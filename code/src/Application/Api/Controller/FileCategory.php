<?php
namespace Application\Api\Controller;

class FileCategory extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_FILECATEGORY);
    }

    public function getListAction()
    {
        $service = $this->getService();
        $list = $service->getList();
        $listNew = [];
        foreach($list as $v){
            $listNew[] = [
                'key' => $v->id,
                'text' => $v->dry_name,
                'class' => ''
            ];
        }
        $send = [
            'list' => $listNew
        ];
        $this->sendSuccess($send);
    }

}