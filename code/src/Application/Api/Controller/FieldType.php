<?php
namespace Application\Api\Controller;

class FieldType extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_FIELDTYPE);
    }

    public function indexAction()
    {
        $data = $this->getRequestData();
        $send = $this->getService()->getIndex($data['page'], $data['limit']);
        $this->sendSuccess($send);
    }

    public function newAction()
    {
        $service = $this->getService();
        $data = $this->getRequestData();
        $data = addDateTime($data, THE_TIME);
        $id = $service->add($data);
        $send = [
            'id' => $id
        ];
        $this->sendSuccess($send);
    }

    public function editAction()
    {
        $service = $this->getService();
        $data = $this->getRequestData();
        $id = $data['id'];
        unset($data['id']);
        $rows = $service->update($data, $id);
        $send = [
            'rows' => $rows
        ];
        $this->sendSuccess($send);
    }

    public function getListAction()
    {
        $service = $this->getService();
        $list = $service->getList();
        $listNew = [];
        foreach($list as $v){
            $listNew[] = [
                'key' => $v->id,
                'text' => $v->dry_name,
                'class' => 'layui-bg-gray'
            ];
        }
        $send = [
            'list' => $listNew
        ];
        $this->sendSuccess($send);
    }

}