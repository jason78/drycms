<?php
namespace Application\Api\Controller;

class Choice extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_CHOICE);
    }

    public function getListByKeyAction()
    {
        $data = $this->getRequestData();
        $service = $this->getService();
        $list = $service->getListByKey($data['key']);
        $result = [];
        foreach($list as $item){
            $class = '';
            if(!empty($item->dry_class)){
                $class = $item->dry_class;
            }
            $result[] = ['key' => $item->dry_key, 'text' => $item->dry_value, 'class' => $class];
        }
        $send = [
            'list' => $result
        ];
        return $this->sendSuccess($send);
    }

}