<?php
namespace Application\Api\Controller;

class Tool extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_TOOL);
    }

    public function push2languageAction()
    {
        $this->getService()->push2language();
        $this->sendSuccess();
    }

    public function language2fileAction()
    {
        $this->getService()->language2file();
        $this->sendSuccess();
    }

    public function choice2fileAction()
    {
        $this->getService()->choice2file();
        $this->sendSuccess();
    }

    public function constant2fileAction()
    {
        $this->getService()->constant2file();
        $this->sendSuccess();
    }

}