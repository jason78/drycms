<?php
namespace Application\Api\Controller;

class Alipay extends Base
{

    /*
        /Api/Alipay/getPay
    */
    public function getPayAction()
    {
        $name = '测试';
        $orderNo = makeOrderNo(632, 0);
        $amount = 0.01;
        $result = $this->get(PAY_ALIPAY)->getPay($name, $orderNo, $amount);
        $this->sendSuccess($result);
    }

}