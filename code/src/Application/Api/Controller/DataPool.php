<?php
namespace Application\Api\Controller;

class DataPool extends Base
{

    public function getDataAction()
    {
        $data = $this->getRequestData();
        $name = $data['name'];
        $serviceDataPool = $this->get(SERVICE_AUTOMATE)->setAlias('data_pool');
        $dataPool = $serviceDataPool->getOneByField('dry_name', $name);
        if($dataPool->dry_domain == '*'){
            $dataPool->dry_domain = getDomain();
        }
        $source = $dataPool->dry_data_source;
        if($source == 1){
            $this->jump("{$dataPool->dry_domain}{$dataPool->dry_api}");
        }
        else{
            $this->jump("{$dataPool->dry_domain}{$dataPool->dry_api}?name={$name}");
        }
    }

}