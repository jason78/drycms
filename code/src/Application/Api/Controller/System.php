<?php
namespace Application\Api\Controller;

class System extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_SYSTEM);
    }

    public function getDatabasesAction()
    {
        $service = $this->getService();
        $list = $service->getDatabases();
        $listNew = [];
        foreach($list as $v){
            $listNew[] = [
                'key' => $v,
                'text' => $v,
                'class' => ''
            ];
        }
        $send = [
            'list' => $listNew
        ];
        $this->sendSuccess($send);
    }

    public function getDatabaseConfigKeysAction()
    {
        $service = $this->getService();
        $list = $service->getDatabaseConfigKeys();
        $listNew = [];
        foreach($list as $v){
            $listNew[] = [
                'key' => $v,
                'text' => $v,
                'class' => ''
            ];
        }
        $send = [
            'list' => $listNew
        ];
        $this->sendSuccess($send);
    }

}