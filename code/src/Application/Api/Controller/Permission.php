<?php
namespace Application\Api\Controller;

class Permission extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_PERMISSION);
    }

    public function getListAction()
    {
        $service = $this->getService();
        $list = $service->getPermission();
        print_r($list);
        $this->sendSuccess();
    }

}