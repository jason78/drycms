<?php
namespace Application\Admin\Controller;

class ModelField extends Base
{

    public function init()
    {
        $this->setDir('model_field');
    }

    public function _initRoute()
    {
        $data = [
            'path_new' => '/Admin/ModelField/new',
            'path_edit' => '/Admin/ModelField/edit',
            'path_index' => '/Admin/ModelField/index',
            'path_show' => '/Admin/ModelField/show',
            'path_delete' => '/Admin/ModelField/delete'
        ];
        $this->setCommonData($data);
    }

    private function getService()
    {
        return $this->get(SERVICE_MODELFIELD);
    }

    public function indexAction()
    {
        $this->setFile($this->getTemplateIndex());
        $this->show();
    }

    public function newAction()
    {
        $this->setFile($this->getTemplateNew());
        $this->setData(['action' => 'new']);
        $this->show();
    }

    public function editAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        $this->setFile($this->getTemplateEdit());
        $this->setData(['action' => 'edit', 'rs' => $rs]);
        $this->show();
    }

    public function showAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        /**/
        $field = $this->get(SERVICE_FIELD)->one($rs->dry_field);
        $rs->dry_database_field = $field->dry_field;
        $rs->dry_form_type = $field->dry_form_type;
        /**/
        $this->setFile($this->getTemplateShow());
        $this->setData(['action' => 'show', 'rs' => $rs]);
        $this->show();
    }

}