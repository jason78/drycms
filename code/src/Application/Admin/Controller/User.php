<?php
namespace Application\Admin\Controller;

class User extends Base
{

    public function init()
    {
        $this->setDir('user');
        $this->setTemplate('updatePassword', 'updatePassword');
    }

    public function updatePasswordAction()
    {
        $this->setFile($this->getTemplate('updatePassword'));
        $this->show();
    }

}