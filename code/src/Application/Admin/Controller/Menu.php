<?php
namespace Application\Admin\Controller;

class Menu extends Base
{

    public function init()
    {
        $this->setDir('menu');
    }

    public function _initRoute()
    {
        $data = [
            'path_new' => '/Admin/Menu/new',
            'path_edit' => '/Admin/Menu/edit',
            'path_index' => '/Admin/Menu/index',
            'path_show' => '/Admin/Menu/show',
            'path_delete' => '/Admin/Menu/delete'
        ];
        $this->setCommonData($data);
    }

    private function getService()
    {
        return $this->get(SERVICE_MENU);
    }

    public function indexAction()
    {
        $this->setFile($this->getTemplateIndex());
        $this->show();
    }

    public function newAction()
    {
        $this->setFile($this->getTemplateNew());
        $this->setData(['action' => 'new']);
        $this->show();
    }

    public function editAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        $this->setFile($this->getTemplateEdit());
        $this->setData(['action' => 'edit', 'rs' => $rs]);
        $this->show();
    }

    public function showAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        $this->setFile($this->getTemplateShow());
        $this->setData(['action' => 'show', 'rs' => $rs]);
        $this->show();
    }

}