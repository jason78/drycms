<?php
namespace Application\Admin\Controller;

class Group extends Base
{

    public function init()
    {
        $this->setDir('group');
        $this->setTemplate('permissionAssign', 'permissionAssign');
    }

    public function _initRoute()
    {
        $data = [
            'path_index' => '/Admin/Automate/index?alias=group'
        ];
        $this->setCommonData($data);
    }

    private function getService()
    {
        return $this->get(SERVICE_GROUP);
    }

    public function permissionAssignAction()
    {
        $data = $this->getRequestData();
        $id = $data['id'];
        $group = $this->get(SERVICE_GROUP)->one($id);
        $servicePermission = $this->get(SERVICE_PERMISSION);
        $list = $servicePermission->getPermissionWithLanguage();
        $assignList = $this->get(SERVICE_GROUPPERMISSION)->getMoreByGroup($id);
        $assigned = array2stringByKey($assignList, 'dry_route_name');
        $this->setFile($this->getTemplate('permissionAssign'));
        $send = [
            'group' => $group,
            'id' => csrfEncrypt($group->id),
            'list' => $list,
            'assigned' => $assigned
        ];
        $this->setData($send);
        $this->show();
    }

}