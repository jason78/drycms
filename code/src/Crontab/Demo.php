<?php
namespace Crontab;

class Demo extends Base
{

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.run '{"name":"drycms"}'
    */
    public function run($data = [])
    {
        print_r($data);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.api
    */
    public function api()
    {
        $data = [
            'key' => 'status'
        ];
        $response = $this->setPath('Api/Choice/getListByKey')->setData($data)->setSign(1)->setMethod('get')->getResponse();
        $this->print_r_ln($response);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.addUserBank
    */
    public function addUserBank()
    {
        $data = [
            'device' => 'sp',
            'token' => '',
            'name' => '',
            'idCard' => '',
            'bankCard' => '',
            'phone' => ''
        ];
        $response = $this->setPath('Api/UserBank/add')->setData($data)->setSign(1)->setMethod('get')->getResponse();
        $this->print_r_ln($response);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.mail
    */
    public function mail()
    {
        $serviceMail = $this->get(SERVICE_MAIL);
        $serviceMail->send('这是主题', '<p style="color:red;">文字</p>', ['test@qq.com']);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.log
    */
    public function log()
    {
        $serviceLog = $this->get(SERVICE_LOG);
        $serviceLog->get('debug')->info('info');
        $serviceLog->get('debug')->error('error');
        $serviceLog->get('debug')->warning('warning');
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.apiLimit
    */
    public function apiLimit()
    {
        $serviceApiLimit = $this->get(SERVICE_APILIMIT)->init();
        $serviceApiLimit->setKey('test')->setTime(10)->setPer(10)->setTotal(1);
        if(!$serviceApiLimit->check()){
            echo '错误' . PHP_EOL;
            exit;
        }
        $serviceApiLimit->add();
        echo '正确' . PHP_EOL;
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.globalId
    */
    public function globalId()
    {
        $serviceGlobalId = $this->get(SERVICE_GLOBALID);
        $id = $serviceGlobalId->getId('dry_test');
        echo $id . PHP_EOL;
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.lock
    */
    public function lock()
    {
        $serviceLock = $this->get(SERVICE_LOCK);
        $name = 'test';
        if($serviceLock->isFirst($name, 20)){
            echo '第一个抢到资源的使用权' . PHP_EOL;
            sleep(20);
            $serviceLock->removeLock($name);
        }
        else{
            echo '您需要等待别人释放资源的使用权' . PHP_EOL;
        }
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.addAccount
    */
    public function addAccount()
    {
        $serviceUserAccount = $this->get(SERVICE_USERACCOUNT);
        $userId = 8;
        $addOrSub = ACCOUNT_SUB;
        $type = ACCOUNT_LOG_TYPE_SYSTEM_SUB;
        $amount = 5;
        $note = '测试';
        $more = [];
        $bool = $serviceUserAccount->addAccount($userId, $addOrSub, $type, $amount, $note, $more);
        var_dump($bool);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.addIncome
    */
    public function addIncome()
    {
        $serviceUserIncomeLog = $this->get(SERVICE_USERINCOMELOG);
        $userId = 8;
        $addOrSub = ACCOUNT_SUB;
        $type = ACCOUNT_LOG_TYPE_SYSTEM_SUB;
        $amount = 5;
        $note = '测试';
        $more = [];
        $id = $serviceUserIncomeLog->addIncome($userId, $addOrSub, $type, $amount, $note, $more);
        var_dump($id);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.bankQuery
    */
    public function bankQuery()
    {
        $serviceFinance = $this->get(SERVICE_FINANCE);
        $result = $serviceFinance->bankQuery('银行卡', 'true');
        print_r($result);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.oss
    */
    public function oss()
    {
        $serviceOssSimple = $this->get(SERVICE_OSSSIMPLE)->setConfig('test', true);
        $result = $serviceOssSimple->upload();
        print_r($result);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.tokenBucket01
    */
    public function tokenBucket01()
    {
        $serviceTokenBucket = $this->get(SERVICE_TOKENBUCKET)->init();
        $serviceTokenBucket->reset();
        for($i = 1; $i <= 10; $i++){
            var_dump($serviceTokenBucket->getToken());
        }
        $n = $serviceTokenBucket->add(10);
        var_dump($n);
        for($i = 1; $i <= 10; $i++){
            var_dump($serviceTokenBucket->getToken());
        }
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.publish
    */
    public function publish()
    {
        $list = ['192.168.21.16:4150'];
        $serviceNsq = $this->get(SERVICE_NSQ);
        $serviceNsq->publish($list, 'test', 'from php');
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.subscribe
    */
    public function subscribe()
    {
        $cb = function($msg)
        {
            print_r($msg);
            echo PHP_EOL;
        };

        $list = ['192.168.21.16:4150'];
        $serviceNsq = $this->get(SERVICE_NSQ);
        $serviceNsq->subscribe($list, 'test', 'channel', $cb);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.host
    */
    public function host()
    {
        $host = getConfig('host', THE_DEFAULT);
        print_r($host);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.redlock
    */
    public function redlock()
    {
        $servers = [
            ['192.168.21.16', 6379, 0.01]
        ];
        $redlock = $this->get(SERVICE_REDLOCK)->init($servers);
        $lock = $redlock->lock('resource_name', 1000);
        print_r($lock);
        $redlock->unlock($lock);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.makeToken
    */
    public function makeToken()
    {
        $device = 'html';
        $userId = 8;
        $token = $this->get(SERVICE_USER)->setToken($device, $userId);
        echo $token;
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Demo.password
    */
    public function password()
    {
        $password = $this->get(CORE_PASSWORD)->passwordHash('drycms@3721');
        var_dump($password);
    }

}