<?php
function getAccountLogTypeText($index)
{
    $map = [
        ACCOUNT_LOG_TYPE_SYSTEM_ADD => '系统增加',
        ACCOUNT_LOG_TYPE_SYSTEM_SUB => '系统减少',
        ACCOUNT_LOG_TYPE_WITHDRAW => '提现',
        ACCOUNT_LOG_TYPE_WITHDRAW_RETURN => '提现返回',
        ACCOUNT_LOG_TYPE_SALE_INCOME => '销售收益'
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getCommonText($index)
{
    $map = [
        CSS_JS_VERSION => '',
        OSS_DOMAIN => 'OSS的域名'
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getDeviceText($index)
{
    $map = [
        DEVICE_ADMIN => '',
        DEVICE_WEIXIN_SP => ''
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getFeedbackStatusText($index)
{
    $map = [
        FEEDBACK_STATUS_NO_REPLY => '',
        FEEDBACK_STATUS_REPLY => ''
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getFeedbackTypeText($index)
{
    $map = [
        FEEDBACK_TYPE_BUG => '',
        FEEDBACK_TYPE_SUGGEST => '',
        FEEDBACK_TYPE_OTHER => ''
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getOrderStatusText($index)
{
    $map = [
        ORDER_STATUS_NO_PAY => '待确认',
        ORDER_STATUS_PAY => '已支付',
        ORDER_STATUS_CANCEL => '已取消',
        ORDER_STATUS_FINISH => '已完成'
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getPayTypeText($index)
{
    $map = [
        PAY_TYPE_ALIPAY => '支付宝',
        PAY_TYPE_WEIXIN => '微信',
        PAY_TYPE_WEIXIN_SP => '微信小程序'
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getWidthText($index)
{
    $map = [
        ID_WIDTH => '',
        SORT_WIDTH => '',
        STATUS_WIDTH => '',
        ADD_TIME_WIDTH => '',
        OPERATE_WIDTH => '',
        DATA_SORT_WIDTH => '',
        ICON_WIDTH => '',
        GRADE_WIDTH => '',
        LINK_WIDTH => '',
        PHONE_WIDTH => '',
        FILE_POSTER_WIDTH => '',
        COLOR_WIDTH => '',
        ORDER_NO_WIDTH => '',
        WIDTH_100 => '',
        WIDTH_110 => '',
        WIDTH_120 => '',
        WIDTH_130 => '',
        WIDTH_140 => '',
        WIDTH_150 => '',
        WIDTH_160 => '',
        WIDTH_170 => '',
        WIDTH_180 => '',
        WIDTH_190 => '',
        WIDTH_200 => '',
        WIDTH_210 => '',
        WIDTH_220 => '',
        WIDTH_230 => '',
        WIDTH_240 => '',
        WIDTH_250 => ''
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getWithdrawStatusText($index)
{
    $map = [
        WITHDRAW_STATUS_APPLY => '待审核',
        WITHDRAW_STATUS_AGREE => '已通过',
        WITHDRAW_STATUS_REFUSE => '已拒绝',
        WITHDRAW_STATUS_PAY => '已打款',
        WITHDRAW_STATUS_FINISH => '已完成'
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}

function getWithdrawTypeText($index)
{
    $map = [
        WITHDRAW_TYPE_ALIPAY => '支付宝',
        WITHDRAW_TYPE_WEI_XIN => '微信',
        WITHDRAW_TYPE_BANK_CARD => '银行卡'
    ];
    if(isset($map[$index])){
        return $map[$index];
    }
    else{
        return '未知';
    }
}
