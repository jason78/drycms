<?php
/*微信的钱单位是分*/
function money2weixin($amount)
{
    return (int)($amount * 100);
}

function makeOrderNo($userId, $type = 0)
{
    $now = date('YmdHis', getTimestamp());
    $rand = mt_rand(1000, 9999);
    $type = padding0($type, 2);
    $userId = padding0($userId, 10);
    return "{$now}{$type}{$userId}{$rand}";
}

/*订单是否过了支付时间*/
function isPayTimeout($orderTime)
{
    $end = strtotime($orderTime) + ORDER_TIME;
    return getTimestamp() > $end;
}

function formatPrice($i)
{
    return number_format($i, 2);
}

function getSensitiveIdCard($idCard)
{
    $length = strlen($idCard);
    return substr($idCard, 0, 6) . str_repeat('*', $length - 10) . substr($idCard, $length - 4);
}

function getSensitiveBankCard($bankCard)
{
    $length = strlen($bankCard);
    return substr($bankCard, 0, 6) . str_repeat('*', $length - 10) . substr($bankCard, $length - 4);
}