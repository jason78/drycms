<?php
function checkAccountLogType($index)
{
    $in = [
        ACCOUNT_LOG_TYPE_SYSTEM_ADD,
        ACCOUNT_LOG_TYPE_SYSTEM_SUB,
        ACCOUNT_LOG_TYPE_WITHDRAW,
        ACCOUNT_LOG_TYPE_WITHDRAW_RETURN,
        ACCOUNT_LOG_TYPE_SALE_INCOME
    ];
    return in_array($index, $in);
}

function checkCommon($index)
{
    $in = [
        CSS_JS_VERSION,
        OSS_DOMAIN
    ];
    return in_array($index, $in);
}

function checkDevice($index)
{
    $in = [
        DEVICE_ADMIN,
        DEVICE_WEIXIN_SP
    ];
    return in_array($index, $in);
}

function checkFeedbackStatus($index)
{
    $in = [
        FEEDBACK_STATUS_NO_REPLY,
        FEEDBACK_STATUS_REPLY
    ];
    return in_array($index, $in);
}

function checkFeedbackType($index)
{
    $in = [
        FEEDBACK_TYPE_BUG,
        FEEDBACK_TYPE_SUGGEST,
        FEEDBACK_TYPE_OTHER
    ];
    return in_array($index, $in);
}

function checkOrderStatus($index)
{
    $in = [
        ORDER_STATUS_NO_PAY,
        ORDER_STATUS_PAY,
        ORDER_STATUS_CANCEL,
        ORDER_STATUS_FINISH
    ];
    return in_array($index, $in);
}

function checkPayType($index)
{
    $in = [
        PAY_TYPE_ALIPAY,
        PAY_TYPE_WEIXIN,
        PAY_TYPE_WEIXIN_SP
    ];
    return in_array($index, $in);
}

function checkWidth($index)
{
    $in = [
        ID_WIDTH,
        SORT_WIDTH,
        STATUS_WIDTH,
        ADD_TIME_WIDTH,
        OPERATE_WIDTH,
        DATA_SORT_WIDTH,
        ICON_WIDTH,
        GRADE_WIDTH,
        LINK_WIDTH,
        PHONE_WIDTH,
        FILE_POSTER_WIDTH,
        COLOR_WIDTH,
        ORDER_NO_WIDTH,
        WIDTH_100,
        WIDTH_110,
        WIDTH_120,
        WIDTH_130,
        WIDTH_140,
        WIDTH_150,
        WIDTH_160,
        WIDTH_170,
        WIDTH_180,
        WIDTH_190,
        WIDTH_200,
        WIDTH_210,
        WIDTH_220,
        WIDTH_230,
        WIDTH_240,
        WIDTH_250
    ];
    return in_array($index, $in);
}

function checkWithdrawStatus($index)
{
    $in = [
        WITHDRAW_STATUS_APPLY,
        WITHDRAW_STATUS_AGREE,
        WITHDRAW_STATUS_REFUSE,
        WITHDRAW_STATUS_PAY,
        WITHDRAW_STATUS_FINISH
    ];
    return in_array($index, $in);
}

function checkWithdrawType($index)
{
    $in = [
        WITHDRAW_TYPE_ALIPAY,
        WITHDRAW_TYPE_WEI_XIN,
        WITHDRAW_TYPE_BANK_CARD
    ];
    return in_array($index, $in);
}
