<?php
namespace Service;

class Choice extends Base
{

    public function getDatabase()
    {
        return $this->get(DATABASE_CHOICE);
    }

    public function getListByKey($key)
    {
        $database = $this->getDatabase();
        return $database->getListByKey($key);
    }

}