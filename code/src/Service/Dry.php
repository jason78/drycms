<?php
namespace Service;

class Dry extends Base
{

    public function makeButton($idIn = '')
    {
        $serviceAutomate = $this->get(SERVICE_AUTOMATE)->setAlias('button');
        $list = $serviceAutomate->getList($idIn);
        $map = [];
        foreach($list as $v){
            $map[$v->dry_position][] = $v;
        }
        foreach($map as $k => $v){
            $map[$k] = orderByField($v, 'dry_sort', SORT_ASC);
        }
        $buttonList = [];
        foreach($map as $k => $v){
            foreach($v as $kk => $vv){
                $config = [
                    'tableLeftTop' => 'layui-btn-sm',
                    'cardRightTop' => 'layui-btn-sm dry-right',
                    'dataRow' => 'layui-btn-xs'
                ];
                $class = $config[$vv->dry_position];
                $button = "<a href='javascript:void(0);' class='layui-btn {$class} {$vv->dry_theme} {$vv->dry_position}' lay-event='{$vv->dry_event}'>{$vv->dry_text}</a>";
                $javascript = [];
                if($vv->dry_position == 'tableLeftTop'){
                    $javascript[] = "f.{$vv->dry_event} = function(object, checkStatus)";
                    $javascript[] = "{";
                    $javascript[] = $vv->dry_code;
                    $javascript[] = "};";
                }
                else if($vv->dry_position == 'cardRightTop'){
                    $javascript[] = "f.{$vv->dry_event} = function()";
                    $javascript[] = "{";
                    $javascript[] = $vv->dry_code;
                    $javascript[] = "};";
                }
                else if($vv->dry_position == 'dataRow'){
                    $javascript[] = "f.{$vv->dry_event} = function(object, id)";
                    $javascript[] = "{";
                    $javascript[] = $vv->dry_code;
                    $javascript[] = "};";
                }
                $javascript = implode(PHP_EOL, $javascript);
                $buttonList[$k]['button'][$kk] = $button;
                $buttonList[$k]['javascript'][$kk] = $javascript;
            }
        }
        return $buttonList;
    }

    public function getSubUserList($userId, $withSelf = true)
    {
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $where = [
            'dry_user' => $userId,
            'dry_status' => 1
        ];
        $subUserList = $serviceAutomate->setAlias('sub_user')->getMoreByFields($where);
        $userIdIn = [];
        if($withSelf){
            $userIdIn[] = $userId;
        }
        if(!empty($subUserList)){
            $list = array2arrayByKey($subUserList, 'dry_sub_user');
            $userIdIn = array_merge($userIdIn, $list);
        }
        return $userIdIn;
    }

    /*白名单*/
    public function getPermissionWhiteList($username = '')
    {
        $where = [
            'dry_username' => $username,
            'dry_has' => 1,
            'dry_status' => 1
        ];
        return $this->get(SERVICE_AUTOMATE)->setAlias('permission_config')->getMoreByFields($where);
    }

    /*黑名单*/
    public function getPermissionBlackList($username = '')
    {
        $where = [
            'dry_username' => $username,
            'dry_has' => 0,
            'dry_status' => 1
        ];
        return $this->get(SERVICE_AUTOMATE)->setAlias('permission_config')->getMoreByFields($where);
    }

    public function makeSearchDefaultValue($data = '')
    {
        $defaultValue = ['', ''];
        $temp = explode('|*|', $data);
        if(isset($temp[0])){
            $defaultValue[0] = $this->makeSearchDefaultValueInner($temp[0]);
        }
        if(isset($temp[1])){
            $defaultValue[1] = $this->makeSearchDefaultValueInner($temp[1]);
        }
        return $defaultValue;
    }

    public function makeSearchDefaultValueInner($data = '')
    {
        if(!startWith($data, '#')){
            return $data;
        }
        $data = substr($data, 1);
        $now = getDateTime();
        $monthInfo = getMonthInfo();
        if($data == 'date_time_start_long'){
            return '1970-01-01 00:00:00';
        }
        else if($data == 'date_time_end_long'){
            return '2030-12-31 23:59:59';
        }
        else if($data == 'date_start_long'){
            return '1970-01-01';
        }
        else if($data == 'date_end_long'){
            return '2030-12-31';
        }
        else if($data == 'time_start'){
            return '00:00:00';
        }
        else if($data == 'time_end'){
            return '23:59:59';
        }
        else if($data == 'date_time_now'){
            return $now['date_time'];
        }
        else if($data == 'time_now'){
            return $now['time'];
        }
        else if($data == 'yesterday'){
            return getYestoday();
        }
        else if($data == 'today'){
            return $now['date'];
        }
        else if($data == 'tomorrow'){
            return getTomorrow();
        }
        else if($data == 'month_first'){
            return $monthInfo['start'];
        }
        else if($data == 'month_last'){
            return $monthInfo['end'];
        }
        else{
            return $data;
        }
    }

    /*生成搜索代码*/
    public function makeSearch($list = [])
    {
        $html = [];
        $json = [
            'input' => [],
            'select' => []
        ];
        $input = [
            'id',
            'input',
            'textarea',
            'date',
            'time',
            'datetime',
            'editor'
        ];
        $select = [
            'radio',
            'checkbox',
            'checkboxBig',
            'select',
            'transfer',
            'switch'
        ];
        foreach($list as $modelField){
            $defaultValue = $this->makeSearchDefaultValue($modelField->dry_field_search_default);
            $field = $modelField->dry_field_data;
            if(in_array($field->dry_form_type, $input)){
                $html[] = $this->makeSearchInput($modelField, $field, $defaultValue);
                $fieldName = $field->dry_field;
                if($modelField->dry_is_encrypt){
                    $fieldName = '*' . $fieldName;
                }
                $json['input'][] = [
                    'field' => $fieldName,
                    'id' => [
                        "{$field->dry_field}_sql_operate",
                        "{$field->dry_field}_value_0",
                        "{$field->dry_field}_value_1"
                    ]
                ];
            }
            else if(in_array($field->dry_form_type, $select)){
                $html[] = $this->makeSearchSelect($modelField, $field, $defaultValue);
                $json['select'][] = [
                    'field' => $field->dry_field,
                    'id' => [
                        $field->dry_field
                    ]
                ];
            }
        }
        $result = [
            'html' => implode(PHP_EOL, $html),
            'json' => json_encode($json)
        ];
        return $result;
    }

    /*生成搜索代码*/
    public function makeSearchInput($modelField, $field, $defaultValue)
    {
        $html = [];
        $html[] = "<div class='layui-col-md4'>";
        $html[] = "<div class='layui-row layui-col-space5'>";
        $html[] = "<div class='layui-col-md2 search-item'>{$field->dry_name}</div>";
        $html[] = "<div class='layui-col-md2'><dry-common role='selectOnly' flag='sql_operate' data-field-name='{$field->dry_field}' name='{$field->dry_field}_sql_operate' value='{$modelField->dry_sql_operate}' url='/Api/DataPool/getData?name=sqlOperate'></dry-common></div>";
        if($field->dry_form_type == 'date'){
            $html[] = "<div class='layui-col-md4'><dry-common role='dateOnly' data-field-name='{$field->dry_field}' name='{$field->dry_field}_value_0' value='{$defaultValue[0]}'></dry-common></div>";
            $html[] = "<div class='layui-col-md4'><dry-common role='dateOnly' data-field-name='{$field->dry_field}' name='{$field->dry_field}_value_1' value='{$defaultValue[1]}'></dry-common></div>";
        }
        else if($field->dry_form_type == 'time'){
            $html[] = "<div class='layui-col-md4'><dry-common role='timeOnly' data-field-name='{$field->dry_field}' name='{$field->dry_field}_value_0' value='{$defaultValue[0]}'></dry-common></div>";
            $html[] = "<div class='layui-col-md4'><dry-common role='timeOnly' data-field-name='{$field->dry_field}' name='{$field->dry_field}_value_1' value='{$defaultValue[1]}'></dry-common></div>";
        }
        else if($field->dry_form_type == 'datetime'){
            $html[] = "<div class='layui-col-md4'><dry-common role='datetimeOnly' data-field-name='{$field->dry_field}' name='{$field->dry_field}_value_0' value='{$defaultValue[0]}'></dry-common></div>";
            $html[] = "<div class='layui-col-md4'><dry-common role='datetimeOnly' data-field-name='{$field->dry_field}' name='{$field->dry_field}_value_1' value='{$defaultValue[1]}'></dry-common></div>";
        }
        else{
            $html[] = "<div class='layui-col-md4'><input type='text' class='layui-input' name='{$field->dry_field}_value_0' id='{$field->dry_field}_value_0' value='{$defaultValue[0]}'></div>";
            $html[] = "<div class='layui-col-md4'><input type='text' class='layui-input' name='{$field->dry_field}_value_1' id='{$field->dry_field}_value_1' value='{$defaultValue[1]}'></div>";
        }
        $html[] = "</div>";
        $html[] = "</div>";
        return implode(PHP_EOL, $html);
    }

    /*生成搜索代码*/
    public function makeSearchSelect($modelField, $field, $defaultValue)
    {
        $attr = $this->get(SERVICE_AUTOMATE)->makeVariableOrUrl($field);
        $html = [];
        $html[] = "<div class='layui-col-md4'>";
        $html[] = "<div class='layui-row layui-col-space5'>";
        $html[] = "<div class='layui-col-md2 search-item'>{$field->dry_name}</div>";
        $html[] = "<div class='layui-col-md2'><dry-common role='selectOnly' scene='{$modelField->dry_search_first_choice}' name='{$field->dry_field}' value='{$defaultValue[0]}' {$attr}></dry-common></div>";
        $html[] = "</div>";
        $html[] = "</div>";
        return implode(PHP_EOL, $html);
    }

    public function language($name)
    {
        $language = $this->getInstance('language');
        $language = $language[THE_DEFAULT];
        if(isset($language[$name])){
            return $language[$name];
        }
        return $name;
    }

}