<?php
namespace Service;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log
{

    public function get($channel = 'debug')
    {
        $config = getConfig('log', THE_DEFAULT);
        $config = $config[$channel];
        $config['file'] = str_replace('@hour', getDateHourInt(), $config['file']);
        $log = new Logger($channel);
        $log->pushHandler(new StreamHandler($config['file'], $config['code']));
        return $log;
    }

}