<?php
namespace Service;

class Callback extends Base
{
    /*
        A=Before|After
        B=New|Edit|EditForShow|Index
        方法名=别名+A+B
    */

    private function gradeManage($data, $serviceAutomate)
    {
        if($data['dry_parent'] == 0){
            $data['dry_grade'] = 1;
        }
        else{
            $rs = $serviceAutomate->one($data['dry_parent']);
            $data['dry_grade'] = $rs->dry_grade + 1;
        }
        return $data;
    }

    /*添加之前数据的处理*/
    public function choiceBeforeNew($alias, $data, $serviceAutomate)
    {
        return $this->gradeManage($data, $serviceAutomate);
    }

    public function choiceAfterNew($alias, $id, $serviceAutomate)
    {
        $config = [
            'table' => $serviceAutomate->getTable()
        ];
        $sqls = getCategoryUpdateSortSql($config);
        $serviceAutomate->setDataSort($sqls);
        return $id;
    }

    public function choiceAfterEdit($alias, $id, $serviceAutomate)
    {
        $config = [
            'table' => $serviceAutomate->getTable()
        ];
        $sqls = getCategoryUpdateSortSql($config);
        $serviceAutomate->setDataSort($sqls);
        return $id;
    }

    public function userBeforeNew($alias, $data, $serviceAutomate)
    {
        $coreEncrypt = $this->get(CORE_ENCRYPT);
        $corePassword = $this->get(CORE_PASSWORD);
        $data['dry_phone'] = $coreEncrypt->setKey('sensitive')->encrypt($data['dry_phone']);
        $data['dry_email'] = $coreEncrypt->setKey('sensitive')->encrypt($data['dry_email']);
        $data['dry_password'] = $corePassword->passwordHash($data['dry_password']);
        $data = addDateTime($data, THE_DATE);
        return $data;
    }

    public function userAfterIndex($alias, $data, $serviceAutomate)
    {
        $coreEncrypt = $this->get(CORE_ENCRYPT);
        $list = $data['list'];
        foreach($list as $k => $item){
            $item->dry_phone = $coreEncrypt->setKey('sensitive')->decrypt($item->dry_phone);
            $item->dry_email = $coreEncrypt->setKey('sensitive')->decrypt($item->dry_email);
            $item->dry_password = '已加密';
            $list[$k] = $item;
        }
        $data['list'] = $list;
        return $data;
    }

    public function userBeforeEditForShow($alias, $data, $serviceAutomate)
    {
        $coreEncrypt = $this->get(CORE_ENCRYPT);
        $data->dry_phone = $coreEncrypt->setKey('sensitive')->decrypt($data->dry_phone);
        $data->dry_email = $coreEncrypt->setKey('sensitive')->decrypt($data->dry_email);
        $data->dry_password = '';
        return $data;
    }

    public function userBeforeEdit($alias, $data, $serviceAutomate)
    {
        $coreEncrypt = $this->get(CORE_ENCRYPT);
        $corePassword = $this->get(CORE_PASSWORD);
        $data['dry_phone'] = $coreEncrypt->setKey('sensitive')->encrypt($data['dry_phone']);
        $data['dry_email'] = $coreEncrypt->setKey('sensitive')->encrypt($data['dry_email']);
        $data['dry_password'] = $corePassword->passwordHash($data['dry_password']);
        return $data;
    }

}