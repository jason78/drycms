<?php
namespace Service;

class FieldType extends Base
{

    public function getDatabase()
    {
        return $this->get(DATABASE_FIELDTYPE);
    }

}