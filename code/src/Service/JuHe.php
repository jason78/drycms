<?php
namespace Service;

class JuHe extends Base
{

    private function getBank4ElementConfig()
    {
        return getConfig('juhe', 'bank4element');
    }

    public function bank4element($bankCard, $name, $idCard, $phone)
    {
        $config = $this->getBank4ElementConfig();
        $appKey = $config['app_key'];
        $url = $config['url'];
        $parameter = [
            'key' => $appKey,
            'bankcard' => $bankCard,
            'realname' => $name,
            'idcard' => $idCard,
            'mobile' => $phone
        ];
        $query = http_build_query($parameter);
        $urlFull = "{$url}?{$query}";
        $response = sendGetByCurl($urlFull);
        return $response;
    }

}