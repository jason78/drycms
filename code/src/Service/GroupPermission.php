<?php
namespace Service;

class GroupPermission extends Base
{

    public function getDatabase()
    {
        return $this->get(DATABASE_GROUPPERMISSION);
    }

    public function deleteByGroup($group = 0)
    {
        $database = $this->getDatabase();
        return $database->deleteByField('dry_group', $group);
    }

    public function getMoreByGroup($group = 0)
    {
        $database = $this->getDatabase();
        return $database->getMoreByField('dry_group', $group);
    }

}