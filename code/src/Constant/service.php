<?php
define('CACHE_LOCK', 'cache.lock');
define('CACHE_TOKENBUCKET', 'cache.tokenbucket');
define('CACHE_MODEL', 'cache.model');
define('CACHE_APILIMIT', 'cache.apilimit');
define('CACHE_BASE', 'cache.base');
define('CACHE_QRLOGIN', 'cache.qrlogin');
define('CACHE_COMMON', 'cache.common');
define('CACHE_MESSAGECODE', 'cache.messagecode');
define('CACHE_GLOBALID', 'cache.globalid');
define('CACHE_USERTOKEN', 'cache.usertoken');
define('CORE_PDO', 'core.pdo');
define('CORE_EVENT', 'core.event');
define('CORE_SQL', 'core.sql');
define('CORE_IMAGEVALIDCODE', 'core.imagevalidcode');
define('CORE_PASSWORD', 'core.password');
define('CORE_REDIS', 'core.redis');
define('CORE_ENCRYPT', 'core.encrypt');
define('CORE_TWIG', 'core.twig');
define('DATABASE_DOCUMENT', 'database.document');
define('DATABASE_USERACCOUNTLOG', 'database.useraccountlog');
define('DATABASE_DOWNLOAD', 'database.download');
define('DATABASE_SYSTEMTASK', 'database.systemtask');
define('DATABASE_GROUP', 'database.group');
define('DATABASE_PRODUCT', 'database.product');
define('DATABASE_ORDER', 'database.order');
define('DATABASE_BANNER', 'database.banner');
define('DATABASE_USERBANK', 'database.userbank');
define('DATABASE_FILE', 'database.file');
define('DATABASE_USER', 'database.user');
define('DATABASE_FIELDTYPE', 'database.fieldtype');
define('DATABASE_MODEL', 'database.model');
define('DATABASE_FIELD', 'database.field');
define('DATABASE_AUTOMATE', 'database.automate');
define('DATABASE_WITHDRAW', 'database.withdraw');
define('DATABASE_USERINCOMELOG', 'database.userincomelog');
define('DATABASE_CHOICE', 'database.choice');
define('DATABASE_MENU', 'database.menu');
define('DATABASE_MODELFIELD', 'database.modelfield');
define('DATABASE_PRODUCTAGENT', 'database.productagent');
define('DATABASE_GROUPPERMISSION', 'database.grouppermission');
define('DATABASE_BASE', 'database.base');
define('DATABASE_BANK4ELEMENT', 'database.bank4element');
define('DATABASE_BANK', 'database.bank');
define('DATABASE_OSSFILE', 'database.ossfile');
define('DATABASE_USERACCOUNT', 'database.useraccount');
define('DATABASE_FEEDBACK', 'database.feedback');
define('DATABASE_PERMISSION', 'database.permission');
define('DATABASE_FILECATEGORY', 'database.filecategory');
define('MODEL_DOCUMENT', 'model.document');
define('MODEL_USERACCOUNTLOG', 'model.useraccountlog');
define('MODEL_DOWNLOAD', 'model.download');
define('MODEL_SYSTEMTASK', 'model.systemtask');
define('MODEL_GROUP', 'model.group');
define('MODEL_PRODUCT', 'model.product');
define('MODEL_ORDER', 'model.order');
define('MODEL_BANNER', 'model.banner');
define('MODEL_USERBANK', 'model.userbank');
define('MODEL_FILE', 'model.file');
define('MODEL_USER', 'model.user');
define('MODEL_FIELDTYPE', 'model.fieldtype');
define('MODEL_MODEL', 'model.model');
define('MODEL_FIELD', 'model.field');
define('MODEL_AUTOMATE', 'model.automate');
define('MODEL_WITHDRAW', 'model.withdraw');
define('MODEL_USERINCOMELOG', 'model.userincomelog');
define('MODEL_CHOICE', 'model.choice');
define('MODEL_MENU', 'model.menu');
define('MODEL_MODELFIELD', 'model.modelfield');
define('MODEL_PRODUCTAGENT', 'model.productagent');
define('MODEL_GROUPPERMISSION', 'model.grouppermission');
define('MODEL_BASE', 'model.base');
define('MODEL_BANK4ELEMENT', 'model.bank4element');
define('MODEL_BANK', 'model.bank');
define('MODEL_OSSFILE', 'model.ossfile');
define('MODEL_USERACCOUNT', 'model.useraccount');
define('MODEL_FEEDBACK', 'model.feedback');
define('MODEL_PERMISSION', 'model.permission');
define('MODEL_FILECATEGORY', 'model.filecategory');
define('PAY_BASE', 'pay.base');
define('PAY_ALIPAY', 'pay.alipay');
define('PAY_WEIXINSP', 'pay.weixinsp');
define('SERVICE_MALLCATEGORY', 'service.mallcategory');
define('SERVICE_TOOL', 'service.tool');
define('SERVICE_CATEGORY', 'service.category');
define('SERVICE_DOCUMENT', 'service.document');
define('SERVICE_USERACCOUNTLOG', 'service.useraccountlog');
define('SERVICE_DOWNLOAD', 'service.download');
define('SERVICE_SYSTEMTASK', 'service.systemtask');
define('SERVICE_OSS', 'service.oss');
define('SERVICE_RESPONSEMASK', 'service.responsemask');
define('SERVICE_GROUP', 'service.group');
define('SERVICE_MAIL', 'service.mail');
define('SERVICE_PRODUCT', 'service.product');
define('SERVICE_ORDER', 'service.order');
define('SERVICE_BANNER', 'service.banner');
define('SERVICE_LOCK', 'service.lock');
define('SERVICE_USERBANK', 'service.userbank');
define('SERVICE_DRY', 'service.dry');
define('SERVICE_WEIXINSPUSER', 'service.weixinspuser');
define('SERVICE_FILE', 'service.file');
define('SERVICE_TOKENBUCKET', 'service.tokenbucket');
define('SERVICE_USER', 'service.user');
define('SERVICE_FIELDTYPE', 'service.fieldtype');
define('SERVICE_NSQ', 'service.nsq');
define('SERVICE_MODEL', 'service.model');
define('SERVICE_FIELD', 'service.field');
define('SERVICE_APILIMIT', 'service.apilimit');
define('SERVICE_SYSTEM', 'service.system');
define('SERVICE_AUTOMATE', 'service.automate');
define('SERVICE_OSSSIMPLE', 'service.osssimple');
define('SERVICE_WITHDRAW', 'service.withdraw');
define('SERVICE_SIGN', 'service.sign');
define('SERVICE_USERINCOMELOG', 'service.userincomelog');
define('SERVICE_CHOICE', 'service.choice');
define('SERVICE_MENU', 'service.menu');
define('SERVICE_MODELFIELD', 'service.modelfield');
define('SERVICE_PRODUCTAGENT', 'service.productagent');
define('SERVICE_GROUPPERMISSION', 'service.grouppermission');
define('SERVICE_JUHE', 'service.juhe');
define('SERVICE_REDLOCK', 'service.redlock');
define('SERVICE_BASE', 'service.base');
define('SERVICE_BANK4ELEMENT', 'service.bank4element');
define('SERVICE_LOG', 'service.log');
define('SERVICE_BANK', 'service.bank');
define('SERVICE_MESSAGECODE', 'service.messagecode');
define('SERVICE_OSSFILE', 'service.ossfile');
define('SERVICE_USERACCOUNT', 'service.useraccount');
define('SERVICE_FEEDBACK', 'service.feedback');
define('SERVICE_PERMISSION', 'service.permission');
define('SERVICE_GLOBALID', 'service.globalid');
define('SERVICE_CALLBACK', 'service.callback');
define('SERVICE_FILECATEGORY', 'service.filecategory');
define('SERVICE_FINANCE', 'service.finance');
define('APPLICATION_API_CONTROLLER_MALLCATEGORY', 'application.api.controller.mallcategory');
define('APPLICATION_API_CONTROLLER_TOOL', 'application.api.controller.tool');
define('APPLICATION_API_CONTROLLER_DOCUMENT', 'application.api.controller.document');
define('APPLICATION_API_CONTROLLER_USERACCOUNTLOG', 'application.api.controller.useraccountlog');
define('APPLICATION_API_CONTROLLER_DOWNLOAD', 'application.api.controller.download');
define('APPLICATION_API_CONTROLLER_SYSTEMTASK', 'application.api.controller.systemtask');
define('APPLICATION_API_CONTROLLER_WEIXINSPPAY', 'application.api.controller.weixinsppay');
define('APPLICATION_API_CONTROLLER_PRODUCT', 'application.api.controller.product');
define('APPLICATION_API_CONTROLLER_ORDER', 'application.api.controller.order');
define('APPLICATION_API_CONTROLLER_BANNER', 'application.api.controller.banner');
define('APPLICATION_API_CONTROLLER_USERBANK', 'application.api.controller.userbank');
define('APPLICATION_API_CONTROLLER_WEIXINSPUSER', 'application.api.controller.weixinspuser');
define('APPLICATION_API_CONTROLLER_FILE', 'application.api.controller.file');
define('APPLICATION_API_CONTROLLER_USER', 'application.api.controller.user');
define('APPLICATION_API_CONTROLLER_FIELDTYPE', 'application.api.controller.fieldtype');
define('APPLICATION_API_CONTROLLER_MODEL', 'application.api.controller.model');
define('APPLICATION_API_CONTROLLER_FIELD', 'application.api.controller.field');
define('APPLICATION_API_CONTROLLER_IMAGEVALIDCODE', 'application.api.controller.imagevalidcode');
define('APPLICATION_API_CONTROLLER_SYSTEM', 'application.api.controller.system');
define('APPLICATION_API_CONTROLLER_AUTOMATE', 'application.api.controller.automate');
define('APPLICATION_API_CONTROLLER_WITHDRAW', 'application.api.controller.withdraw');
define('APPLICATION_API_CONTROLLER_DATAPOOL', 'application.api.controller.datapool');
define('APPLICATION_API_CONTROLLER_USERINCOMELOG', 'application.api.controller.userincomelog');
define('APPLICATION_API_CONTROLLER_CHOICE', 'application.api.controller.choice');
define('APPLICATION_API_CONTROLLER_MENU', 'application.api.controller.menu');
define('APPLICATION_API_CONTROLLER_MODELFIELD', 'application.api.controller.modelfield');
define('APPLICATION_API_CONTROLLER_WEIXINSPPAY2', 'application.api.controller.weixinsppay2');
define('APPLICATION_API_CONTROLLER_GROUPPERMISSION', 'application.api.controller.grouppermission');
define('APPLICATION_API_CONTROLLER_ALIPAY', 'application.api.controller.alipay');
define('APPLICATION_API_CONTROLLER_BANK', 'application.api.controller.bank');
define('APPLICATION_API_CONTROLLER_QRLOGIN', 'application.api.controller.qrlogin');
define('APPLICATION_API_CONTROLLER_MESSAGECODE', 'application.api.controller.messagecode');
define('APPLICATION_API_CONTROLLER_FEEDBACK', 'application.api.controller.feedback');
define('APPLICATION_API_CONTROLLER_PERMISSION', 'application.api.controller.permission');
define('APPLICATION_API_CONTROLLER_FILECATEGORY', 'application.api.controller.filecategory');