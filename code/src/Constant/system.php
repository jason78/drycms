<?php
define('URL_ERROR', 'URL_ERROR');
define('CLASS_ERROR', 'CLASS_ERROR');
define('METHOD_ERROR', 'METHOD_ERROR');
define('THE_DEFAULT', 'default');
define('THE_NONE', 'none');
define('THE_DATE', 1);
define('THE_TIME', 2);
define('THE_DATE_TIME', 3);
define('THE_DATE_FORMAT', 'Y-m-d');
define('THE_TIME_FORMAT', 'H:i:s');
define('THE_DATE_TIME_FORMAT', 'Y-m-d H:i:s');
define('THE_DATE_TIME_START_LONG', '1970-01-01 00:00:00');
define('THE_DATE_TIME_END_LONG', '2030-12-31 23:59:59');
define('MINUTE_SECOND', 60);
define('MINUTE_5', 300);
define('HALF_HOUR_SECOND', 1800);
define('HOUR_SECOND', 3600);
define('DAY_SECOND', 86400);
define('WEEK_SECOND', 604800);
define('MONTH_SECOND', 2592000);
define('TOKEN_TIME', 864000);
define('WITHDRAW_SPAN_TIME', 360);
define('HOME_URL', 'https://www.drycms.com');
/*订单多少时间不支付就取消(秒)*/
define('ORDER_TIME', 900);
/*sql类型*/
define('SQL_INSERT', 1);
define('SQL_UPDATE', 2);
/*金额加减类型*/
define('ACCOUNT_ADD', 1);
define('ACCOUNT_SUB', 2);