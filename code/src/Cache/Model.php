<?php
namespace Cache;

class Model extends Base
{

    private $prefix = 'model';

    public function __construct()
    {
        $this->redis = $this->get(CORE_REDIS)->get('model');
    }

    public function getKey($name)
    {
        return sprintf('%s_%s', $this->prefix, $name);
    }

}