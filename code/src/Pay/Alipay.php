<?php
namespace Pay;
use Yansongda\Pay\Pay;

class Alipay extends Base
{

    private function getPayConfig()
    {
        $temp = getConfig('alipay', 'default');
        $config = [
            'app_id' => $temp['app_id'],
            'notify_url' => $temp['notify_url'],
            'return_url' => 'https://www.drycms.com',
            'ali_public_key' => $temp['alipay_rsa_public_key'],
            'private_key' => $temp['rsa_private_key']
        ];
        return $config;
    }

    /*
        支付信息
    */
    public function getPay($name, $orderNo, $amount)
    {
        $order = [
            'subject' => $name,
            'out_trade_no' => $orderNo,
            'total_amount' => $amount
        ];
        return Pay::alipay($this->getPayConfig())->wap($order)->getContent();
    }

}