<?php
namespace Database;

class FieldType extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_FIELDTYPE);
    }

    public function getIndex($page, $limit, $extension = [])
    {
        return $this->getIndexByOrder($page, $limit, 'dry_sort', 'asc');
    }

}