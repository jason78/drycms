<?php
namespace Database;

class Menu extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_MENU);
    }

    public function getIndex($page, $limit, $extension = [])
    {
        return $this->getIndexByOrder($page, $limit, 'dry_data_sort', 'asc');
    }

    public function getParent()
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->setOrder('dry_data_sort', 'asc');
        $list = $this->fetchAll($sql->get());
        return $list;
    }

}