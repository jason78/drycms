<?php
namespace Core;

class ImageValidCode
{

    private $width;//宽

    private $height;//高

    private $im;//图片资源

    public $code;//验证码字符串

    public $number = 6;//验证码位数

    public $borderColor = '230,230,230';//边框颜色[为空则不显示边框]

    public $backgroundColor = '255,255,255';//背景颜色[必须设置]

    public $character = 4;//字符集[0数字 1小写字母 2大写字母 3字母 4数字字母]

    public $color = '';//字体颜色[255,0,0|0,255,0|0,0,255]为空则随机生成

    public $fontFamily = '';//字体[为空会使用默认字体]

    public $fontSize = 5;//字体大小

    public $space = 15;//文字间隔

    public $marginLeft = 10;//距离左边距离

    public $marginTop = 10;//距离顶部距离

    public $angle = '';//倾斜角度范围[可以为空]

    //设置图片的宽度和高度
    public function setWidthHeight($width = 80, $height = 36)
    {
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    //创建图片资源(相当于创建一个画布)
    private function createImage()
    {
        $this->im = imagecreatetruecolor($this->width, $this->height);
        $data = explode(',', $this->backgroundColor);
        $color = imagecolorallocate($this->im, $data[0], $data[1], $data[2]);
        imagefill($this->im, 0, 0, $color);
        if($this->borderColor != ''){
            $data = explode(',', $this->borderColor);
            $color = imagecolorallocate($this->im, $data[0], $data[1], $data[2]);
            ImageRectangle($this->im, 0, 0, $this->width - 1, $this->height - 1, $color);
        }
    }

    //生成随机验证码(只是记录验证码是什么)
    private function createCode()
    {
        $data = [];
        $data[0] = '123456789';
        $data[1] = 'abcdefghjkmnpqrstuvwxyz';
        $data[2] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        $data[3] = $data[1] . $data[2];
        $data[4] = $data[0] . $data[3];
        for($i = 0;$i < $this->number;$i++){
            $string = $data[$this->character];
            $rand = rand(0, strlen($string) - 1);
            $this->code .= $string{$rand};
        }
    }

    //设置验证码(把验证码画到画布上)
    private function setCode()
    {
        for($i = 0;$i < $this->number;$i++){
            if($this->color != ''){
                $data = explode('|', $this->color);
                $temp = [];
                foreach($data as $key => $value){
                    $temp[$key] = explode(',', $value);
                }
                $inner = $i % count($data);
                $color = imagecolorallocate($this->im, $temp[$inner][0], $temp[$inner][1], $temp[$inner][2]);
            }
            else{
                $color = imagecolorallocate($this->im, rand(0, 255), rand(0, 255), rand(0, 255));
            }
            if($this->fontFamily == ''){
                if($this->fontSize > 5){
                    $this->fontSize = 5;
                }
                imagechar($this->im, $this->fontSize, $i * $this->space + $this->marginLeft, $this->marginTop, $this->code{$i}, $color);
            }
            else{
                if($this->angle != ''){
                    $angle = explode(',', $this->angle);
                    $angle = rand($angle[0], $angle[1]);
                }
                else{
                    $angle = 0;
                }
                imagettftext($this->im, $this->fontSize, $angle, $i * $this->space + $this->marginLeft, $this->fontSize + $this->marginTop, $color, $this->fontFamily, $this->code{$i});
            }
        }
    }

    //得到验证吗(程序需要自己记录方便后续验证)
    public function getCode()
    {
        $this->createImage();
        $this->createCode();
        $this->setCode();
        return $this->code;
    }

    //设置图片
    public function makeImage()
    {
        ob_start();
        imagepng($this->im);
        $content = ob_get_contents();
        ob_end_clean();
        ImageDestroy($this->im);
        return $content;
    }

}