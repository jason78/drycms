<?php
namespace Core;

class Encrypt
{

    private $key = THE_DEFAULT;

    public function setKey($key = '')
    {
        $this->key = $key;
        return $this;
    }

    private function getConfig()
    {
        $config = getConfig('encrypt', $this->key);
        return $config;
    }

    public function encrypt($data)
    {
        $config = $this->getConfig();
        $key = $config['key'];
        $iv = $config['iv'];
        return openssl_encrypt($data, 'aes-128-cbc', $key, 0, $iv);
    }

    public function decrypt($data)
    {
        if($data == ''){
            return '';
        }
        $config = $this->getConfig();
        $key = $config['key'];
        $iv = $config['iv'];
        return openssl_decrypt($data, 'aes-128-cbc', $key, 0, $iv);
    }

}