<?php
namespace Core;

class Password
{

    public function passwordHash($text)
    {
        return password_hash($text, \PASSWORD_DEFAULT);
    }

    public function passwordVerify($text, $hash)
    {
        return password_verify($text, $hash);
    }

}