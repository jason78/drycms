<?php
namespace Core;

class Container
{

    private $pool = [];

    private $instance = [];

    public function set($name, $information)
    {
        $this->pool[$name] = $information;
    }

    public function get($name)
    {
        $information = $this->pool[$name];
        if(isset($information['single']) && $information['single']){
            if(isset($this->instance[$name])){
                return $this->instance[$name];
            }
            else{
                $class = new $information['class']();
                $this->instance[$name] = $class;
                return $class;
            }
        }
        return new $information['class']();
    }

    public function setInstance($name, $instance)
    {
        $this->instance[$name] = $instance;
    }

    public function getInstance($name)
    {
        return $this->instance[$name];
    }

    public function load($file)
    {
        $data = yaml_parse_file($file);
        if(isset($data['services'])){
            foreach($data['services'] as $name => $information){
                $this->set($name, $information);
            }
        }
    }

}