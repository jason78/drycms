<?php
namespace Core;

class Twig extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{

    public function getService($name)
    {
        return getContainer()->get($name);
    }

    public function getGlobals()
    {
        return [
            'test' => 'test'
        ];
    }

    public function getTests()
    {
        return [
            new \Twig_SimpleTest('json_string', function($value){
                $bool = is_array(json_decode($value, true));
                if($bool){
                    return 'yes';
                }
                else{
                    return 'no';
                }
            }),
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_home', function(){
                return HOME_URL;
            }),
            new \Twig_SimpleFunction('constant', function($name){
                $list = get_defined_constants(true);
                return $list['user'][$name];
            }),
            new \Twig_SimpleFunction('language', function($name){
                return $this->getService(SERVICE_DRY)->language($name);
            }),
            new \Twig_SimpleFunction('json_encode', function($data){
                return json_encode($data);
            }),
            new \Twig_SimpleFunction('get_oss_domain', function(){
                return getOssDomain();
            }),
            new \Twig_SimpleFunction('get_domain', function(){
                return getDomain();
            }),
            new \Twig_SimpleFunction('resource', function($type, $in){
                $map = [
                    'css' => [
                        0 => ['flag' => '', 'path' => '/css/card.css'],
                        1 => ['flag' => '', 'path' => '/css/common.css'],
                        2 => ['flag' => '', 'path' => '/css/file.css'],
                        50 => ['flag' => '', 'path' => '/layuiadmin/layui/css/layui.css'],
                        51 => ['flag' => '', 'path' => '/layuiadmin/style/admin.css']
                    ],
                    'images' => [
                        0 => ['flag' => '', 'path' => '/images/file.png'],
                        1 => ['flag' => '', 'path' => '/images/video.png']
                    ],
                    'js' => [
                        0 => ['flag' => '', 'path' => '/js/compressor.js'],
                        1 => ['flag' => '', 'path' => '/js/dry-choice.js'],
                        2 => ['flag' => '', 'path' => '/js/dry-common.js'],
                        3 => ['flag' => '', 'path' => '/js/dry-constant.js'],
                        4 => ['flag' => '', 'path' => '/js/dry-show.js'],
                        50 => ['flag' => '', 'path' => '/layuiadmin/layui/layui.js'],
                        101 => ['flag' => 'defer', 'path' => '/wc/dry-cascade-select.js'],
                        102 => ['flag' => 'defer', 'path' => '/wc/dry-common.js'],
                        103 => ['flag' => 'defer', 'path' => '/wc/dry-navigator-document.js'],
                        104 => ['flag' => 'defer', 'path' => '/wc/dry-navigator.js'],
                        105 => ['flag' => 'defer', 'path' => '/wc/dry-phone-plus.js'],
                        106 => ['flag' => 'defer', 'path' => '/wc/dry-phone.js']
                    ]
                ];
                $in = explode(',', $in);
                $list = $map[$type];
                $code = [];
                foreach($in as $n){
                    $code[] = $type($list[$n]);
                }
                return implode(PHP_EOL, $code);
            }),
        ];
    }

}