function iconShow(value)
{
    return `<i class="layui-icon ${value}"></i>`;
}

function linkShow(value)
{
    if(value == ""){
        return "";
    }
    return `<a href="${value}" class="layui-btn layui-btn-xs layui-btn-normal" target="_blank">click</a>`;
}

function indentShow(value, grade)
{
    function getSpace(theGrade, number)
    {
        var list = [];
        var total = (theGrade - 1) * number;
        for(var i = 1;i <= total;i++){
            list.push('&nbsp;');
        }
        return list.join('');
    }
    
    return getSpace(grade, 5) + value;
}

function htmlShow(html)
{
    if(html == ""){
        return "";
    }
    html = HTMLEncode(html);
    return `<textarea class="layui-hide">${html}</textarea><a href="javascript:void(0);" class="layui-btn layui-btn-xs layui-btn-normal htmlShow">HTML</a>`;
}

function codeShow(code)
{
    if(code == ""){
        return "";
    }
    code = HTMLEncode(code);
    return `<textarea class="layui-hide">${code}</textarea><a href="javascript:void(0);" class="layui-btn layui-btn-xs layui-btn-normal codeShow">CODE</a>`;
}

function colorShow(value)
{
    if(value == ""){
        return "";
    }
    return `<span class="layui-badge-rim" style="background-color:${value};">&nbsp;</span>`;
}

function buttonThemeShow(value)
{
    var map = {
        "layui-btn-green": "墨绿",
        "layui-btn-primary": "银灰",
        "layui-btn-normal": "蓝色",
        "layui-btn-warm": "橙色",
        "layui-btn-danger": "赤色"
    };
    var text = map[value];
    return `<button type="button" class="layui-btn layui-btn-xs ${value}">${text}</button>`;
}

function jsonShow(value)
{
    if(value == ""){
        return "";
    }
    var code = formatJSON(value);
    return `<textarea class="layui-hide">${code}</textarea><a href="javascript:void(0);" class="layui-btn layui-btn-xs layui-btn-normal codeShow">JSON</a>`;
}