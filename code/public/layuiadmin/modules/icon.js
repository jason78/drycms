layui.define(['layer', 'jquery', 'laytpl'], function(exports){
    var layer = layui.layer;
    var $ = layui.jquery;
    var laytpl = layui.laytpl;
    var code = `
    <div class="layui-fluid">
        <div class="layui-row">
            {{# layui.each(d.list, function(index, item){ }}
                <div class="layui-col-md1 dry-icon-box" data="{{ item }}">
                {{# if(item == d.defaultValue){ }}
                    <i class="dry-icon-item layui-icon {{ item }}" style="color:red;"></i>
                {{# }else{ }}
                    <i class="dry-icon-item layui-icon {{ item }}" style="color:black;"></i>
                {{# } }}
                </div>
            {{# }); }}
        </div>
    </div>
    `;
    var json = `
    {
        "code": 0,
        "msg": "",
        "data": {
            "list": [
                "layui-icon-rate-half",
                "layui-icon-rate",
                "layui-icon-rate-solid",
                "layui-icon-cellphone",
                "layui-icon-vercode",
                "layui-icon-login-wechat",
                "layui-icon-login-qq",
                "layui-icon-login-weibo",
                "layui-icon-password",
                "layui-icon-username",
                "layui-icon-refresh-3",
                "layui-icon-auz",
                "layui-icon-spread-left",
                "layui-icon-shrink-right",
                "layui-icon-snowflake",
                "layui-icon-tips",
                "layui-icon-note",
                "layui-icon-home",
                "layui-icon-senior",
                "layui-icon-refresh",
                "layui-icon-refresh-1",
                "layui-icon-flag",
                "layui-icon-theme",
                "layui-icon-notice",
                "layui-icon-website",
                "layui-icon-console",
                "layui-icon-face-surprised",
                "layui-icon-set",
                "layui-icon-template-1",
                "layui-icon-app",
                "layui-icon-template",
                "layui-icon-praise",
                "layui-icon-tread",
                "layui-icon-male",
                "layui-icon-female",
                "layui-icon-camera",
                "layui-icon-camera-fill",
                "layui-icon-more",
                "layui-icon-more-vertical",
                "layui-icon-rmb",
                "layui-icon-dollar",
                "layui-icon-diamond",
                "layui-icon-fire",
                "layui-icon-return",
                "layui-icon-location",
                "layui-icon-read",
                "layui-icon-survey",
                "layui-icon-face-smile",
                "layui-icon-face-cry",
                "layui-icon-cart-simple",
                "layui-icon-cart",
                "layui-icon-next",
                "layui-icon-prev",
                "layui-icon-upload-drag",
                "layui-icon-upload",
                "layui-icon-download-circle",
                "layui-icon-component",
                "layui-icon-file-b",
                "layui-icon-user",
                "layui-icon-find-fill",
                "layui-icon-loading",
                "layui-icon-loading-1",
                "layui-icon-add-1",
                "layui-icon-play",
                "layui-icon-pause",
                "layui-icon-headset",
                "layui-icon-video",
                "layui-icon-voice",
                "layui-icon-speaker",
                "layui-icon-fonts-del",
                "layui-icon-fonts-code",
                "layui-icon-fonts-html",
                "layui-icon-fonts-strong",
                "layui-icon-unlink",
                "layui-icon-picture",
                "layui-icon-link",
                "layui-icon-face-smile-b",
                "layui-icon-align-left",
                "layui-icon-align-right",
                "layui-icon-align-center",
                "layui-icon-fonts-u",
                "layui-icon-fonts-i",
                "layui-icon-tabs",
                "layui-icon-radio",
                "layui-icon-circle",
                "layui-icon-edit",
                "layui-icon-share",
                "layui-icon-delete",
                "layui-icon-form",
                "layui-icon-cellphone-fine",
                "layui-icon-dialogue",
                "layui-icon-fonts-clear",
                "layui-icon-layer",
                "layui-icon-date",
                "layui-icon-water",
                "layui-icon-code-circle",
                "layui-icon-carousel",
                "layui-icon-prev-circle",
                "layui-icon-layouts",
                "layui-icon-util",
                "layui-icon-templeate-1",
                "layui-icon-upload-circle",
                "layui-icon-tree",
                "layui-icon-table",
                "layui-icon-chart",
                "layui-icon-chart-screen",
                "layui-icon-engine",
                "layui-icon-triangle-d",
                "layui-icon-triangle-r",
                "layui-icon-file",
                "layui-icon-set-sm",
                "layui-icon-add-circle",
                "layui-icon-404",
                "layui-icon-about",
                "layui-icon-up",
                "layui-icon-down",
                "layui-icon-left",
                "layui-icon-right",
                "layui-icon-circle-dot",
                "layui-icon-search",
                "layui-icon-set-fill",
                "layui-icon-group",
                "layui-icon-friends",
                "layui-icon-reply-fill",
                "layui-icon-menu-fill",
                "layui-icon-log",
                "layui-icon-picture-fine",
                "layui-icon-face-smile-fine",
                "layui-icon-list",
                "layui-icon-release",
                "layui-icon-ok",
                "layui-icon-help",
                "layui-icon-chat",
                "layui-icon-top",
                "layui-icon-star",
                "layui-icon-star-fill",
                "layui-icon-close-fill",
                "layui-icon-close",
                "layui-icon-ok-circle",
                "layui-icon-add-circle-fine"
            ]
        }
    }
    `;

    function set(boxId)
    {
        (function(){
            var box = null;
            var id = null;
            var defaultValue = "";

            function setIcon(icon)
            {
                box.html("<i class='dry-input-line-height layui-icon @icon'></i>".replace("@icon", icon));
                $("#" + id).val(icon);
            }

            function setHover()
            {
                $(".dry-icon-box").hover(
                    function(){
                        $(this).css("background-color", "#e6e6e6");
                    },
                    function(){
                        $(this).css("background-color", "transparent");
                    }
                );
            }

            function setClick()
            {
                $(".dry-icon-box").click(function(){
                    var icon = $(this).attr("data");
                    setIcon(icon);
                    defaultValue = icon;
                    $("i.dry-icon-item").css("color", "black");
                    $(this).find("i.dry-icon-item").css("color", "red");
                });
            }

            box = $("#" + boxId);
            id = box.attr("data-id");
            defaultValue = $("#" + id).val();
            if(defaultValue == ""){
                box.html("<div class='dry-input-line-height'>请点击后选择</div>");
            }
            else{
                setIcon(defaultValue);
            }
            box.click(function(){
                var iconData = JSON.parse(json);
                var data = iconData.data;
                data.defaultValue = defaultValue;
                layer.open({type: 1, skin: 'layui-layer-rim', area: ['654px', '697px'], content: laytpl(code).render(data)});
                setHover();
                setClick();
            });
        })();
    }

    exports('icon', {set: set});
});