class dryNavigatorDocument extends HTMLElement{
    constructor(){
        super();
        var that = this;
        var domain = get("domain");
        var url = get("url");
        var ref = get("ref");
        var callback = get("callback");
        var shrink = get("shrink");
        var unique = get("unique");
        var filter = get("filter");
        var vclass = get("class");
        var fold = get("fold");
        var icon = get("icon");
        
        function get(name, theDefault = "")
        {
            if(that.hasAttribute(name)){
                return that.getAttribute(name);
            }
            return theDefault;
        }
        
        function $$(id)
        {
            return document.getElementById(id);
        }
        
        function getUnique()
        {
            var d = new Date();
            var temp = Math.random() + "";
            return "g_" + d.getTime() + "_" + temp.substr(2);
        }
        
        function getRequest(url, successCallback, failCallback)
        {
            function success(response)
            {
                if(response.ok == true && response.status == 200){
                    response.json().then(function(json){
                        successCallback(json);
                    });
                }
            }
            
            function fail(response)
            {
                if(failCallback){
                    failCallback(response);
                }
            }
            
            fetch(url).then(success).catch(fail);
        }
        
        function getCode(layui)
        {
            var code = `
            {{#  layui.each(d.list, function(index, item){ }}
                {{#  if(item.dry_content != '' && item.dry_content != null){ }}
                    <div class="content">{{ item.dry_content }}</div>
                {{#  } }}
                
                {{#  if(item.dry_image_ext.length){ }}
                    <div class="layui-card">
                        <div class="layui-card-header layui-unselect" style="background-color:#009688;color:white;">图片集</div>
                        <div class="layui-card-body" style="border:1px solid #009688;margin-top:-1px;">
                            <div class="layui-row layui-col-space10">
                                {{#  layui.each(item.dry_image_ext, function(index2, image){ }}
                                <div class="layui-col-xs3 layui-col-sm3 layui-col-md3 layui-col-lg3">
                                    <div style="border:1px solid #e6e6e6;padding:2px;">
                                        <a href="javascript:void(0);">
                                            <div><img class="photo-swipe" group="{{ item.id }}" src="{{ d.oss_domain }}/{{ image.dry_object }}" width="100%" index="{{ index2 }}" w="{{ image.dry_width }}" h="{{ image.dry_height }}"></div>
                                            <div class="center">{{ image.dry_name }}</div>
                                        </a>
                                    </div>
                                </div>
                                {{#  }); }}
                            </div>
                        </div>
                    </div>
                {{#  } }}
                
                {{#
                if(item.dry_video_ext.length){
                    var ids = [];
                    layui.each(item.dry_video_ext, function(index, video){
                        ids.push(video.id);
                    });
                    var idIn = ids.join(",");
                }}
                    <div class="layui-card">
                        <div class="layui-card-header layui-unselect" style="background-color:#009688;color:white;">视频集</div>
                        <div class="layui-card-body" style="border:1px solid #009688;margin-top:-1px;">
                            <div class="dryMergeRequest" role="file" field="file" url="@domain/Api/File/getListByIds" iframeUrl="" full="1">{{ idIn }}</div>
                        </div>
                    </div>
                {{#  } }}
                
                {{#  if(item.dry_code != '' && item.dry_code != null){ }}
                    <pre class="layui-code" lay-title="代码" lay-height="200" lay-skin="" lay-encode="">
                    {{ HTMLEncode(item.dry_code) }}
                    </pre>
                {{#  } }}
                
            {{#  }); }}
            `;
            return code;
        }
        
        function commonCallback(y)
        {
            function success(json)
            {
                var code = getCode(y);
                code = code.replace("@domain", domain);
                laytpl(code).render(json.data, function(html){
                    html = doCode(html);
                    $("body").unbind("click");
                    $(".dry-right-content").html(html);
                    y.dry.mergeRequest();
                    y.code({"about": false});
                    swipe();
                });
            }
            
            function swipe()
            {
                $("body").on("click", ".photo-swipe", function(e){
                    var element = $(e.target);
                    var group = element.attr("group");
                    var index = parseInt(element.attr("index"));
                    var items = [];
                    $(".photo-swipe[group='" + group + "']").each(function(){
                        var element2 = $(this);
                        var item = {
                            src: element2.attr("src"),
                            w: element2.attr("w"),
                            h: element2.attr("h")
                        };
                        items.push(item);
                    });
                    show(items, index);
                });
            }
            
            var $ = y.jquery;
            var laytpl = y.laytpl;
            $("dd a").click(function(){
                if($(this).hasClass("active")){
                    return false;
                }
                $("dd a").removeClass("active");
                $(this).addClass("active");
                getRequest($(this).attr("lay-href"), success);
            });
            y.element.render();
            $(".layui-nav dd a").eq(0).trigger("click");
        }
        
        function runCallback(p)
        {
            var f = window[callback];
            if(!f){
                setTimeout(function(){
                    runCallback(p);
                }, 500);
            }
            else{
                f(p);
                that.setAttribute("ok", "1");
            }
        }

        function parse(data)
        {
            var children = [];
            var hasParent = [];
            var topList = [];
            
            function hasChildren(item)
            {
                if(children[item.id] && children[item.id].length){
                    return true;
                }
                return false;
            }
            
            function doChildren(item)
            {
                var ddList = [];
                ddList.push(`<dd>`);
                var a = `<a href='javascript:void(0);' lay-href='@href'>@name</a>`;
                if(hasChildren(item)){
                    a = `<a href='javascript:void(0);'>@name</a>`;
                }
                ddList.push(a.replace("@name", item.dry_name).replace("@href", domain + "/Api/Document/getList?id=" + item.id));
                if(hasChildren(item)){
                    var childrenList = children[item.id];
                    ddList.push(`<dl class='layui-nav-child'>`);
                    for(var j in childrenList){
                        ddList.push(doChildren(childrenList[j]));
                    }
                    ddList.push(`</dl>`);
                }
                ddList.push(`</dd>`);
                return ddList.join("");
            }
            
            for(var i in data){
                var item = data[i];
                hasParent[item.id] = false;
                if(item.dry_parent > 0){
                    hasParent[item.id] = true;
                    if(!children[item.dry_parent]){
                        children[item.dry_parent] = [];
                    }
                    children[item.dry_parent].push(item);
                }
                if(!hasParent[item.id]){
                    topList.push(item);
                }
            }
            
            var liList = [];
            for(var i in topList){
                var item = topList[i];
                var cssClass = "";
                if(fold == "0"){
                    cssClass = "layui-nav-itemed";
                }
                liList.push(`<li class='layui-nav-item layui-unselect ${cssClass}'>`);
                var iconCode = `<i class='layui-icon'></i>`;
                if(icon == "0"){
                    iconCode = "";
                }
                liList.push(`<a href='javascript:void(0);'>${iconCode}<cite>${item.dry_name}</cite></a>`);
                if(hasChildren(item)){
                    var childrenList = children[item.id];
                    liList.push(`<dl class='layui-nav-child'>`);
                    for(var j in childrenList){
                        liList.push(doChildren(childrenList[j]));
                    }
                    liList.push(`</dl>`);
                }
                liList.push(`</li>`);
            }
            
            return liList.join("");
        }
        
        function finish(json)
        {
            var li = parse(json.data);
            var ul = `<ul class="layui-nav ${vclass}" lay-shrink="${shrink}" id="${unique}" lay-filter="${filter}">${li}</ul>`;
            if(ref == ""){
                ref = getUnique();
                that.insertAdjacentHTML("afterend", `<div id="${ref}"></div>`);
            }
            $$(ref).innerHTML = ul;
            runCallback(commonCallback);
        }
        
        if(callback == ""){
            callback = "dryCallback";
        }
        getRequest(url, finish);
    }
};
window.customElements.define('dry-navigator-document', dryNavigatorDocument);