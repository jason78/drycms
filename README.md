### 项目介绍

DRYCMS是一款基于PHP7(swoole)+MYSQL的智能内容管理系统，后台几乎不需要写代码，支持docker部署。

### 文档地址

[文档](https://www.drycms.com)

### 开源协议

MIT

### 演示地址

http://116.62.156.219

用户名 administrator

密码 drycms@3721

每天24点重置一次数据库

### 视频教程

视频列表

https://space.bilibili.com/520869934

建议PC网页1080P观看

`1-DRYCMS的安装`

https://www.bilibili.com/video/BV1gK4y1t7pA

`2-ubuntu18.04安装docker并运行mysql和redis`

https://www.bilibili.com/video/BV1Le411W7bh

`3-DRYCMS功能演示`

https://www.bilibili.com/video/BV18i4y147Ei