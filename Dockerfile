FROM xxfaxy/self-alpine-swoole:latest
RUN sed -i '944a extension=swoole.so' /etc/php7/php.ini
VOLUME "/app"
WORKDIR "/app"
ADD code /app
EXPOSE 80
ADD run.sh /run.sh
RUN chmod +x /run.sh
CMD ["/run.sh"]